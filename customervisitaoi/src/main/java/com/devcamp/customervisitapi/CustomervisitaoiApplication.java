package com.devcamp.customervisitapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CustomervisitaoiApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomervisitaoiApplication.class, args);
	}

}
